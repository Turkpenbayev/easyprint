# EasyPrint

**An complete Django application template, with Django, Celery, RabbitMQ, Docker.**

<!-- Use this Flask app to initiate your project with less work. In this application  template you will find the following plugins already configured: -->


## Requirements

Python 2.5+, python-pip, virtualenv

## Instalation

First, clone this repository:

    $ git clone https://gitlab.com/Turkpenbayev/easyprint.git
    $ cd easyprint

Install Pip on linux:

    $ sudo apt-get install python-pip

To install virtualenv globally with pip (if you have pip 1.3 or greater installed globally):

    $ sudo pip install virtualenv


After, install all necessary to run:

    $ pip install -r requirements.txt

Than, run the application:

	$ python manage.py runserver 0.0.0.0:8000

To see your application, access this url in your browser: 

	http://0.0.0.0:8000

All configuration is in: `printf/settings.py`
