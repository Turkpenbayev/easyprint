from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from student.models import Documents
from django.http import JsonResponse
from django.core import serializers


# Create your views here.

@login_required(login_url = '/login')
def index(request):

    user = request.user
    return render(request, 'dashboard.html', {'user': user})


def doc_search(request):

    if request.method == 'POST':
        code = request.POST['doc_code']

        try:
            document = Documents.objects.filter(code = code)
            data = serializers.serialize('json', document)            
        except Exception as ex:
            print(ex)
            return JsonResponse({'error': 'Документ не найден!'})
            
        try:
            return render(request, 'print.html', {'data': document[0]})
        except Exception as ex:
            return JsonResponse({'error': 'Документ не найден!'})

        
        
        
        

