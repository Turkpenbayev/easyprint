from django.urls import path
from django.contrib.auth import views as auth_views
from . import views


app_name = 'center'

urlpatterns = [
    path('', views.index, name = 'index'),
    path('search', views.doc_search, name = 'doc_search'),

]