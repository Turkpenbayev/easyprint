from django import forms
from .models import Documents

class DocumentForm(forms.ModelForm):
    class Meta:
        model = Documents
        fields = ('document',)
    
    def __init__(self, *args, **kwargs):
        super(DocumentForm, self).__init__(*args, **kwargs)
        self.fields['document'].widget.attrs.update({'class' : 'inputfile inputfile-5', 'id': 'file-upload'})