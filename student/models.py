from django.db import models
from center.models import CenterUser
import os

# Create your models here.

def upload_path(instance, filename):
    return os.path.join(str(instance.__class__.__name__).lower() + '/', filename)

class Documents(models.Model):
    class Meta:
        verbose_name = u'Документ'
        verbose_name_plural = u'Документы'

    STATUS = (
        (1, 'Не печатан'),
        (2, 'Печатан')
    )

    file_name = models.CharField(max_length = 128, blank = True)
    document = models.FileField(upload_to = upload_path)
    code = models.CharField(max_length = 16)
    amount = models.IntegerField(default = 0)
    uploaded_at = models.DateTimeField(auto_now_add = True)
    printed_by = models.CharField(max_length = 128)
    state = models.IntegerField(verbose_name='Статус', choices=STATUS, default=1)

    def __str__(self):
        return self.document.name

    @property
    def filename(self):
        name = self.document.name
        return name
